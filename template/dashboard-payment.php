<?php
/**
 * Template Name: Reservation Payment
 */
if ( !is_user_logged_in() ) {
    wp_redirect(  home_url('/') );
}

get_header();
global $current_user;

wp_get_current_user();
$userID = $current_user->ID;
$reservation_id=$_GET['reservation_id'];
//$reservation_id = $reservation_status = '';
if(isset($_GET['reservation_id']) && !empty($_GET['reservation_id'])) {
    $reservation_id = $_GET['reservation_id'];
    $reservation_status = get_post_meta($reservation_id, 'reservation_status', true);
} 

$enable_paypal = homey_option('enable_paypal');
$enable_billplz = homey_option('enable_billplz');
$enable_stripe = homey_option('enable_stripe');
$stripe_processor_link = homey_get_template_link('template/template-stripe-charge.php');
$is_hourly = get_post_meta($reservation_id, 'is_hourly', true);

global $current_user, $homey_local, $homey_prefix, $reservationID, $owner_info, $renter_info, $renter_id, $owner_id;
$blogInfo = esc_url( home_url('/') );
wp_get_current_user();
$userID =   $current_user->ID;
$back_to_list = homey_get_template_link_2('template/dashboard-reservations.php');
$messages_page = homey_get_template_link_2('template/dashboard-messages.php');
$booking_hide_fields = homey_option('booking_hide_fields');

$reservationID = isset($_GET['reservation_id']) ? $_GET['reservation_id'] : '';
$reservation_status = $notification = $status_label = $notification = '';
$upfront_payment = $check_in = $check_out = $guests = $pets = $renter_msg = '';
$payment_link = '';


    $post = $_GET['reservation_id'];    

    $reservation_status = get_post_meta($reservationID, 'reservation_status', true);
    $upfront_payment = get_post_meta($reservationID, 'reservation_upfront', true);
    $upfront_payment = homey_formatted_price($upfront_payment);
    $payment_link = homey_get_template_link_2('template/dashboard-payment.php');

    $check_in = get_post_meta($reservationID, 'reservation_checkin_date', true);
    $check_out = get_post_meta($reservationID, 'reservation_checkout_date', true);
    $guests = get_post_meta($reservationID, 'reservation_guests', true);
    $listing_id = get_post_meta($reservationID, 'reservation_listing_id', true);
    $pets   = get_post_meta($listing_id, $homey_prefix.'pets', true);
    $res_meta   = get_post_meta($reservationID, 'reservation_meta', true);
$is_hourly=get_post_meta($reservationID, 'is_hourly', true);
    $message = isset($res_meta['renter_msg']) ? $res_meta['renter_msg'] : '';

    $renter_id = get_post_meta($reservationID, 'listing_renter', true);
    $renter_info = homey_get_author_by_id('60', '60', 'reserve-detail-avatar img-circle', $renter_id);

    $owner_id = get_post_meta($reservationID, 'listing_owner', true);
    $owner_info = homey_get_author_by_id('60', '60', 'reserve-detail-avatar img-circle', $owner_id);

    $payment_link = add_query_arg( array(
            'reservation_id' => $reservationID,
        ), $payment_link );

    $chcek_reservation_thread = homey_chcek_reservation_thread($reservationID);

    if($chcek_reservation_thread != '') {
        $messages_page_link = add_query_arg( array(
            'thread_id' => $chcek_reservation_thread
        ), $messages_page );
    } else {
        $messages_page_link = add_query_arg( array(
            'reservation_id' => $reservationID,
            'message' => 'new',
        ), $messages_page );
    }
    

    $guests_label = homey_option('cmn_guest_label');
    if($guests > 1) {
        $guests_label = homey_option('cmn_guests_label');
    }


?>

<section id="body-area">

    <div class="dashboard-page-title">
        <h1><?php the_title(); ?></h1>
    </div><!-- .dashboard-page-title -->

    <?php get_template_part('template-parts/dashboard/side-menu'); ?>

    <div class="user-dashboard-right dashboard-with-sidebar">
        <div class="dashboard-content-area">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12">
                        <form name="homey_checkout" method="post" class="homey_payment_form" action="<?php echo esc_url($stripe_processor_link); ?>">
						
						<input type="hidden" name="check_in_date" id="check_in_date" value="<?php echo $check_in; ?>">
                                    <input type="hidden" name="check_out_date" id="check_out_date" value="<?php echo $check_out; ?>"/>
									<input type="hidden" name="guests" id="guests" value="<?php echo $guests; ?>">
                                    <input type="hidden" name="listing_id" id="listing_id" value="<?php echo $listing_id; ?>"/>
									 <input type="hidden" name="message" id="message" value="<?php echo $message; ?>"/>
									 <input type="hidden" name="extra_options" id="extra_options" value=""/>
									 			 <input type="hidden" name="is_hourly" id="is_hourly" value="0"/>
                                    <input type="hidden" name="reservation_id" id="reservation_id" value="<?php echo intval($reservation_id); ?>">
                                    <input type="hidden" name="checkout-security" id="checkout-security" value="<?php echo wp_create_nonce('checkout-security-nonce'); ?>"/>
                            <div class="dashboard-area">

                                <div class="block">
                                    <div class="block-head">
                                        <div class="block-left">
                                            <h2 class="title"><?php esc_html_e('Select the payment method', 'homey'); ?></h2>
                                        </div><!-- block-left -->
                                    </div><!-- block-head -->
                                
                                    <div class="block-body">
                                        <div class="row">
                                            <div class="col-sm-12">
                                                <div class="payment-method">
                                                    <?php if($enable_paypal != 0) { ?>
                                                    <div class="payment-method-block paypal-method">
                                                        <div class="form-group">
                                                            <label class="control control--radio radio-tab">
                                                                <input name="payment_gateway" value="paypal" type="radio">
                                                                <span class="control-text"><?php esc_html_e('Paypal', 'homey'); ?></span>
                                                                <span class="control__indicator"></span>
                                                                <span class="radio-tab-inner"></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <?php } ?>

                                                    <?php if($enable_billplz != 0) { ?>
                                                    <div class="payment-method-block billplz-method">
                                                        <div class="form-group">
                                                            <label class="control control--radio radio-tab">
                                                                <input name="payment_gateway" value="billplz" type="radio">
                                                                <span class="control-text"><?php esc_html_e('Billplz', 'homey'); ?></span>
                                                                <span class="control__indicator"></span>
                                                                <span class="radio-tab-inner"></span>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <?php } ?>

                                                    <?php if($enable_stripe != 0) { ?>
                                                    <div class="payment-method-block stripe-method">
                                                        <div class="form-group">
                                                            <label class="control control--radio radio-tab">
                                                                <input name="payment_gateway" value="stripe" type="radio">
                                                                <span class="control-text"><?php esc_html_e('Stripe', 'homey'); ?></span>
                                                                <span class="control__indicator"></span>
                                                                <span class="radio-tab-inner"></span>
                                                            </label>
                                                            <?php 
                                                            if($is_hourly == 'yes') {
                                                                homey_hourly_stripe_payment($reservation_id);
                                                            } else {
                                                                homey_stripe_payment($reservation_id);
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                    <?php } ?>
                                               
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                  
                                    <div class="block-section">
                                        <div class="block-body">
                                            <div class="block-left">
                                                <h2 class="title"><?php esc_html_e('Payment', 'homey'); ?></h2>
                                            </div><!-- block-left -->
                                            <div class="block-right">
                                                <?php 
                                                if($is_hourly == 'yes') {
                                                    echo homey_calculate_hourly_reservation_cost($reservation_id);
                                                } else {
                                                    echo homey_calculate_reservation_cost($reservation_id);
                                                }
                                                ?>
                                            </div><!-- block-right -->
                                        </div><!-- block-body -->
                                    </div><!-- block-section -->
                                </div><!-- .block -->

                                <?php if($reservation_status == 'available') { ?>
                                <div class="payment-buttons">
                                    <div id="homey_notify"></div>
									
									
 
						
			

                                    <?php
                                    if($is_hourly == 'yes') { ?>
                                        <button id="make_hourly_booking_payment" class="btn btn-success btn-full-width"><?php esc_html_e('Process Payment', 'homey'); ?></button>
                                    <?php    
                                    } else { ?>
                                        <a id="make_booking_payment12" class="btn btn-success btn-full-width"><?php esc_html_e('Process Payment', 'homey'); ?></a>
                                    <?php
                                    }
                                    ?>
                                    <div id="instance_noti"></div>

                                </div>
                                <?php } ?>
                            </div><!-- .dashboard-area -->
                        </form>
                    </div><!-- col-lg-12 col-md-12 col-sm-12 -->
                </div>
            </div><!-- .container-fluid -->
        </div><!-- .dashboard-content-area -->    
        
    </div><!-- .user-dashboard-right -->

</section><!-- #body-area -->

<?php get_footer();?>
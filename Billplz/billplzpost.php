<?php
require_once("../wp-load.php");

define( 'WP_USE_THEMES', false );  
require('../wp-blog-header.php');

require 'lib/API.php';
require 'lib/Connect.php';
require 'configuration.php';

use Billplz\Minisite\API;
use Billplz\Minisite\Connect;
$current_user = wp_get_current_user();
$userID       =   $current_user->ID;
$user_email   =   $current_user->user_email;
$admin_email  =  get_bloginfo('admin_email');
$username     =   $current_user->user_login;
$submission_currency = homey_option('payment_currency');
$reservation_page_link = homey_get_template_link('template/dashboard-reservations.php');
$add_new_listing = homey_get_template_link('template/dashboard-submission.php');
$paymentMethod='BillPlz';


$date = date( 'Y-m-d g:i:s', current_time( 'timestamp', 0 ));
$parameter = array(
    'collection_id' => !empty($collection_id) ? $collection_id : $_REQUEST['collection_id'],
    'email'=> isset($_REQUEST['email']) ? $_REQUEST['email'] : '',
    'mobile'=> isset($_REQUEST['mobile']) ? $_REQUEST['mobile'] : '',
    'name'=> isset($_REQUEST['name']) ? $_REQUEST['name'] : 'No Name',
    'amount'=>  $_REQUEST['amount'],
	'tesrt'=>'testttttt',
	'callback_url'=> $websiteurl . '/callback.php',
    'description'=> !empty($description) ? $description : $_REQUEST['description']
);


$optional = array(
   'redirect_url' => $websiteurl . '/Billplz/callback.php',
    'reference_1_label' => isset($reference_1_label) ? $reference_1_label : $_REQUEST['reference_1_label'],
    'reference_1' => isset($_REQUEST['reference_1']) ? $_REQUEST['reference_1'] : '',
    'reference_2_label' => isset($reference_2_label) ? $reference_2_label : $_REQUEST['reference_2_label'],
    'reference_2' => isset($_REQUEST['reference_2']) ? $_REQUEST['reference_2'] : '',
    'deliver' => 'false',
	
);

if (empty($parameter['mobile']) && empty($parameter['email'])) {
    $parameter['email'] = 'noreply@billplz.com';
}

if (!filter_var($parameter['email'], FILTER_VALIDATE_EMAIL)) {
    $parameter['email'] = 'noreply@billplz.com';
}

$connnect = (new Connect($api_key))->detectMode();
$billplz = new API($connnect);
list ($rheader, $rbody) = $billplz->toArray($billplz->createBill($parameter));
/***********************************************/
// Include tracking code here
/***********************************************/
echo "<pre>";
print_r($rbody);
/*
if ($rheader == 200) {
	
	if($_POST['update_payment']==1){
		 $listing_id=$_POST['listing_id'];
		 
		$reservation_id=$_POST['reservation_id'];
	//echo  $reservation_id = get_post_id_by_meta_key_and_value('reservation_listing_id', '256');

	
	 update_post_meta( $reservation_id, 'reservation_status', 'booked' );
}
else
{
	$is_instance_booking=$_POST['is_instance_booking'];
$listing_id=$_POST['listing_id'];
$is_hourly=$_POST['is_hourly'];
$is_extra_options =0;


	  if( $is_instance_booking == 0 ) { 
            $listing_id = get_post_meta($reservation_id, 'reservation_listing_id', true );

            
            if($is_hourly == 1) {
                //Book hours
                $booked_days_array = homey_make_hours_booked($listing_id, $reservation_id);
                update_post_meta($listing_id, 'reservation_booked_hours', $booked_days_array);

                //Remove Pending Hours
                $pending_dates_array = homey_remove_booking_pending_hours($listing_id, $reservation_id);
                update_post_meta($listing_id, 'reservation_pending_hours', $pending_dates_array);
                
            } else {
                //Book dates
                $booked_days_array = homey_make_days_booked($listing_id, $reservation_id);
                update_post_meta($listing_id, 'reservation_dates', $booked_days_array);

                //Remove Pending Dates
                $pending_dates_array = homey_remove_booking_pending_days($listing_id, $reservation_id);
                update_post_meta($listing_id, 'reservation_pending_dates', $pending_dates_array);
            }
            
            // Update reservation status
            update_post_meta( $reservation_id, 'reservation_status', 'booked' );

        } elseif( $is_instance_booking == 1 ) {
            $listing_id = $_POST['listing_id'];
            $guests = isset($_POST['guests']) ? $_POST['guests'] : '';
            $renter_message = isset($_POST['guest_message']) ? $_POST['guest_message'] : '';
            $check_in_date = isset($_POST['check_in_date']) ? $_POST['check_in_date'] : '';
            $extra_options_data = '';
            if($is_extra_options == 1) {
                $extra_options_data = get_user_meta($userID, 'extra_prices', true);
            }

            if($is_hourly == 1) {

                $check_in_hour = isset($_POST['check_in_hour']) ? $_POST['check_in_hour'] : '';
                $check_out_hour = isset($_POST['check_out_hour']) ? $_POST['check_out_hour'] : '';
                $start_hour = isset($_POST['start_hour']) ? $_POST['start_hour'] : '';
                $end_hour = isset($_POST['end_hour']) ? $_POST['end_hour'] : '';

                $reservation_id = homey_add_hourly_instance_booking($listing_id, $check_in_date, $check_in_hour, $check_out_hour, $start_hour, $end_hour, $guests, $renter_message, $extra_options_data);

            } else {
                
                $check_out_date = isset($_POST['check_out_date']) ? $_POST['check_out_date'] : '';
                $reservation_id = homey_add_instance_booking($listing_id, $check_in_date, $check_out_date, $guests, $renter_message, $extra_options_data);
            }

            //Create messages thread
            do_action('homey_create_messages_thread', $renter_message, $reservation_id);
        }

}

        //Add host earning history
        homey_add_earning($reservation_id);

        //Add invoice
        $invoiceID = homey_generate_invoice( 'reservation','one_time', $reservation_id, $date, $userID, 0, 0, '', $paymentMethod );
      
        update_post_meta( $invoiceID, 'invoice_payment_status', 1 );

        // Emails
        $listing_owner = get_post_meta($reservation_id, 'listing_owner', true);
        $listing_renter = get_post_meta($reservation_id, 'listing_renter', true);

        $renter = homey_usermeta($listing_renter);
      

        $owner = homey_usermeta($listing_owner);
       
        $email_args = array('reservation_detail_url' => reservation_detail_link($reservation_id) );
        homey_email_composer( $renter_email, 'booked_reservation', $email_args );
        homey_email_composer( $owner_email, 'admin_booked_reservation', $email_args );
        
        $return_link = add_query_arg( 'reservation_detail', $reservation_id, $reservation_page_link );

       wp_redirect( $return_link ); exit;
exit ;
    
}*/
if ($rheader !== 200) {
	///echo "uuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuuu";
	
	
    if (defined('DEBUG')) {
        echo '<pre>'.print_r($rbody, true).'</pre>';
    }
    if (!empty($fallbackurl)) {
        header('Location: ' . $fallbackurl);
    }
}
header('Location: ' . $rbody['url']);

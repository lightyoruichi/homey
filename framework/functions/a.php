/* -----------------------------------------------------------------------------------------------------------
*  Stripe Form
-------------------------------------------------------------------------------------------------------------*/
if( !function_exists('homey_billplz_payment_instance') ) {
    function homey_billplz_payment_instance($listing_id, $check_in, $check_out, $guests) {

        require_once( HOMEY_PLUGIN_PATH . '/includes/billplz/Connect.php' );
        $billplz_secret_key = homey_option('billplz_secret_key');
        $billplz_api = homey_option('billplz_publishable_key');
        $billplz_collectionID = homey_option('billplz_collectionID');
        $allowded_html = array();

        $billplz = array(
            "secret_key" => $billplz_secret_key,
            "publishable_key" => $billplz_publishable_key
        );

        \Stripe\Stripe::setApiKey($stripe['secret_key']);
        $submission_currency = homey_option('payment_currency');
        $current_user = wp_get_current_user();
        $userID = $current_user->ID;
        $userfname = $current_user->user_firstname; 
        $user_email = $current_user->user_email;

        $listing_id     = intval($listing_id);
        $check_in_date  = wp_kses ($check_in, $allowded_html);
        $check_out_date = wp_kses ($check_out, $allowded_html);
        $renter_message = '';//$renter_message;
        $guests         = intval($guests);

        $extra_options = isset($_GET['extra_options']) ? $_GET['extra_options'] : '';

        update_user_meta($userID, 'extra_prices', $extra_options); 

        if(!empty($extra_options)) {
            $extra_prices = 1;
        } else {
            $extra_prices = 0;
        }

        $check_availability = check_booking_availability($check_in_date, $check_out_date, $listing_id, $guests);
        $is_available = $check_availability['success'];
        $check_message = $check_availability['message'];

        if(!$is_available) {

            echo json_encode( 
                array( 
                    'success' => false, 
                    'message' => $check_message,
                    'payment_execute_url' => ''
                ) 
             );
            wp_die();

        } else {

            $prices_array = homey_get_prices($check_in_date, $check_out_date, $listing_id, $guests, $extra_options);
            $upfront_payment  =  floatval( $prices_array['upfront_payment'] );
        }

        if( $submission_currency == 'JPY') {
            $upfront_payment = $upfront_payment;
        } else {
            $upfront_payment = $upfront_payment * 100;
        }
        

        print '
        <div class="homey_stripe_simple">
            <script src="https://checkout.stripe.com/checkout.js"
            class="stripe-button"
            data-key="' . $stripe_publishable_key . '"
            data-amount="' . $upfront_payment . '"
            data-email="' . $user_email . '"
            data-zip-code="true"
            data-billing-address="true"
            data-locale="'.get_locale().'"
            data-currency="' . $submission_currency . '"
            data-label="' . esc_html__('Pay with Credit Card', 'homey') . '"
            data-description="' . esc_html__('Reservation Payment', 'homey') . '">
            </script>
        </div>
        <input type="hidden" id="reservation_id_for_stripe" name="reservation_id_for_stripe" value="0">
        <input type="hidden" id="reservation_pay" name="reservation_pay" value="1">
        <input type="hidden" id="is_instance_booking" name="is_instance_booking" value="1">
        <input type="hidden" name="check_in_date" value="'.$check_in_date.'">
        <input type="hidden" name="check_out_date" value="'.$check_out_date.'">
        <input type="hidden" name="guests" value="'.$guests.'">
        <input type="hidden" name="listing_id" value="'.$listing_id.'">
        <input type="hidden" name="extra_options" value="'.$extra_prices.'">
        <input type="hidden" id="guest_message" name="guest_message" value="'.$renter_message.'">
        <input type="hidden" name="userID" value="' . $userID . '">
        <input type="hidden" id="pay_ammout" name="pay_ammout" value="' . $upfront_payment . '">


        <form method="post" action="/includes/stripe-php/init.php">
    <input type="text" name='name' required value="' . $userfname . '">
    <input type="text" name='email' required value="' . $user_email . '">
    <input type="hidden" name="mobile" value="60121234567">
    <input type="hidden" name="amount" value ="' . $upfront_payment . '">
    <input type="hidden" name="reference_1_label" value="Listing ID">
    <input type="hidden" name="reference_1" value="'.$listing_id.'">
    <input type="hidden" name="reference_2_label" value="User ID">
    <input type="hidden" name="reference_2" value="' . $userID . '">
    <input type="hidden" name="description" value ="Reservation Payment for '.$listing_id.'">
    <input type="hidden" name="collection_id" value = "' . $stripe_publishable_key . '">
    <input type="submit">
        </form>
        ';
    }
}
